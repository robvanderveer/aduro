//
//  main.m
//  AduroCenter
//
//  Created by Rob van der Veer on 3/8/14.
//  Copyright (c) 2014 Rob van der Veer. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
