//
//  AduroDriver.m
//  AduroCenter
//
//  Created by Rob van der Veer on 3/8/14.
//  Copyright (c) 2014 Rob van der Veer. All rights reserved.
//
// Driver class to send commands to the Aduro

#import "AduroDriver.h"
#import <AppKit/AppKit.h>

@implementation AduroDriver

#define MAXRECEIVEBUFFER 256
#define RESPONSE_TIMEOUT 5
#define CONNECT_TIMEOUT 30

char receiveBuffer[MAXRECEIVEBUFFER];
NSInteger receiveBufferSize;

NSTimer *timeoutWatchdog;

-(instancetype)initWithPath:(NSString *)path
{
    self = [super init];
    
    _path = path;
    [self setState:Closed];

    return self;
}

-(void)dealloc
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    if(_port)
    {
        if(_port.isOpen)
        {
            [_port close];
           
        }
         _port = nil;
    }
}

-(void)connect
{
    if(_port == nil || !_port.isOpen)
    {
        [self setState:Connecting];

        NSLog(@"Attempt open port %@", _path);
        _port = [[ORSSerialPort alloc] initWithPath:_path];
        [_port setBaudRate:@57600];
        [_port setParity:ORSSerialPortParityNone];
        [_port setNumberOfStopBits:1];
        [_port setDTR:true];

        _port.delegate = self;
        [_port open];

        NSLog(@"Open completed with state %ld", _state);

    }
    else
    {
        NSLog(@"Port already opened");
        if(_state == Error)
        {
            [self sendSync];
        }
    }
}

-(void)updateWatchdog:(id)sender
{
    //called every so-so seconds.
    //If we do not receive any data from the serial port within the timer,
    //then board is not responding. Unsync.
}

//verify the port and path, and attempt to reopen if available.
-(bool)verifyPort
{
    switch(_state)
    {
        case Synced:
            return true;
        case Connected:
            [self sendSync];
            return false;
        default:
            return false;
    }
}

#pragma Mark -- Serial Port delegates
-(void)serialPort:(ORSSerialPort *)serialPort didReceiveData:(NSData *)data
{
    if(timeoutWatchdog)
    {
        [timeoutWatchdog invalidate];
        timeoutWatchdog = nil;
    }

    //append the data to the buffer. Once we have a complete line we will handle it.
    const uint8_t *bytes = [data bytes];

    for(int i = 0; i < [data length]; i++)
    {
        uint8_t byte = bytes[i];
        //NSLog(@"** received: %d", byte);

        if(bytes[i] == '\n')
        {
            //terminate:
            receiveBuffer[receiveBufferSize] = 0;
            
            //handle the buffer.
            NSString *received = [NSString stringWithUTF8String:receiveBuffer];
            
            [self processResponse:received];
            
            //reset the buffer.
            receiveBufferSize = 0;
        }
        else if(byte == '\r')
        {
            //ignore.
        }
        else
        {
            //add the data.
            if(receiveBufferSize < MAXRECEIVEBUFFER)
            {
                receiveBuffer[receiveBufferSize++] = byte;
            }
        }
    }
}

-(void)processResponse:(NSString *)received
{
    if([received isEqualToString:@"+Aduro ready"])
    {
        NSLog(@"# Aduro found");
        
        [self setState:Synced];
    }
    else if([received isEqualToString:@"CRC error"])
    {
        NSLog(@"# CRC Error / Sync lost");
        //resync
        [self setState:Error];
        [self sendSync];
    }
    else if([received hasPrefix:@"OK"])
    {
        //we don't need confirmation.
        
        //       _isSynced = true;
        //      [self notifyStatusChanged];
    }
    else
    {
        NSLog(@"< %@", received);
    }
}

-(void)disconnect
{
    //close the ports.
    if(_port.isOpen)
    {
        //shut down!
        [_port close];
    }
    
    _port = nil;
    self.state = Disconnected;
}

-(void)serialPortWasRemovedFromSystem:(ORSSerialPort *)serialPort
{
    //TODO: Act accordingly.
    NSLog(@"Serial port removed");
    
    [self disconnect];
    NSLog(@"Serial port remove completed.");
}

-(void)serialPort:(ORSSerialPort *)serialPort didEncounterError:(NSError *)error
{
    NSLog(@"Serial port error %@", error.localizedDescription);

    [self disconnect];
}

-(void)serialPortWasOpened:(ORSSerialPort *)serialPort
{
    NSLog(@"Serial port opened");
    
    self.state = Connected;
    [self sendSync];
}

-(void)sendSync
{
    NSLog(@"Attempt sync");

    //try to sync.
    uint8_t cmd[1];
    cmd[0] = 'I';
    
    //acceptable is 1 through 100.
    [self sendData:[NSData dataWithBytes:&cmd length:1]];
}

-(void)serialPortWasClosed:(ORSSerialPort *)serialPort
{
    NSLog(@"Serial port closed");
    
    [self disconnect];
}

#pragma Mark --

-(void)sendData:(NSData *)data
{
    //add a timeout.
    //set up watchdog.
    if(timeoutWatchdog)
    {
        [timeoutWatchdog invalidate];
        timeoutWatchdog = nil;
    }
    
    //calculate checksum.
    const uint8_t *bytes = [data bytes];
    uint8_t checksum = 0;
    for (int i = 0; i < [data length]; i++)
    {
        checksum += bytes[i];
    }

    NSMutableData *cmd = [[NSMutableData alloc] init];
    uint8_t header[2];
    header[0] = 0x78;
    header[1] = [data length];
    [cmd appendBytes:header length:2];
    [cmd appendData:data];
    [cmd appendBytes:&checksum length:1];
    
    [_port sendData:cmd];
    
    timeoutWatchdog = [NSTimer scheduledTimerWithTimeInterval:RESPONSE_TIMEOUT
                                     target:self
                                   selector:@selector(responseTimeout:)
                                   userInfo:nil
                                    repeats:NO];

}

-(void)responseTimeout:(id)sender
{
    NSLog(@"Timeout!");
    self.state = Error;
}

-(void)connectTimeout:(id)sender
{
    NSLog(@"Connect Timeout!");

}

-(void)updateBrightness:(NSInteger)level
{
    if([self verifyPort] == false)
    {
        NSLog(@"! port closed");
        return;
    }

    uint8_t cmd[2];
    cmd[0] = 'B';
    cmd[1] = level;
    
    //acceptable is 1 through 100.
    [self sendData:[NSData dataWithBytes:&cmd length:2]];
}

-(void)updateWithColors:(NSArray *)colors
{
    if([self verifyPort] == false)
    {
        NSLog(@"! port closed");
        return;
    }

    NSMutableData *data = [[NSMutableData alloc] init];
    
    NSString *command = @"C";
    [data appendData:[command dataUsingEncoding:NSASCIIStringEncoding]];
    
    NSAssert(colors.count == 60, @"Colors should be 60, not %ld", colors.count );
    
    NSColor *c;
    for(c in colors)
    {
        uint8_t rgb[3];
        rgb[0] = (uint8_t)(c.redComponent * 255);
        rgb[1] = (uint8_t)(c.greenComponent * 255);
        rgb[2] = (uint8_t)(c.blueComponent * 255);
        
        [data appendData:[NSData dataWithBytes:&rgb length:3]];
    }
    
    [self sendData:data];
}

-(void)sendPulse:(NSColor *)color
{
    if([self verifyPort] == false)
    {
        NSLog(@"! port closed");
        return;
    }
    
    NSMutableData *data = [[NSMutableData alloc] init];
    
    NSString *command = @"P";
    [data appendData:[command dataUsingEncoding:NSASCIIStringEncoding]];
    
    //data = [[NSMutableData alloc] init];
    uint8_t rgb[3];
    rgb[0] = (uint8_t)(color.redComponent * 255);
    rgb[1] = (uint8_t)(color.greenComponent * 255);
    rgb[2] = (uint8_t)(color.blueComponent * 255);
        
    [data appendData:[NSData dataWithBytes:&rgb length:3]];
    
    [self sendData:data];
}

-(void)setState:(AduroState)state
{
    if(_state != state)
    {
        _state = state;
        [self notifyStatusChanged:state];
    }
}

-(void)notifyStatusChanged:(AduroState)state
{
    if(_delegate)
    {
        [_delegate blueLink:self ChangedStatus:state];
    }
}

-(void)setPath:(NSString *)path
{
    if(![path isEqualToString:_path])
    {
        NSLog(@"Changing path to %@, reconnecting", path);

        //disconnect, change path and reconnect.
        [self disconnect];
        
        _path = path;
        [self connect];
    }
}

@end
