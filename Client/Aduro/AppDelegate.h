//
//  AppDelegate.h
//  AduroCenter
//
//  Created by Rob van der Veer on 3/8/14.
//  Copyright (c) 2014 Rob van der Veer. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "AduroDriver.h"
#import "SettingsWindowController.h"

@interface AppDelegate : NSObject <NSApplicationDelegate, NSUserNotificationCenterDelegate, AduroDelegate, SettingsDelegate>

@property (nonatomic, strong) NSNumber *debugHidden;

-(IBAction)showPreferences:(id)sender;
-(IBAction)showAbout:(id)sender;
-(IBAction)connect:(id)sender;
-(IBAction)goodPulse:(id)sender;
-(IBAction)badPulse:(id)sender;

@end

