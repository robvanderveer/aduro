//
//  BlinkNegativeCommand.m
//  Aduro
//
//  Created by Rob van der Veer on 22/8/14.
//  Copyright (c) 2014 Rob van der Veer. All rights reserved.
//

#import "BlinkNegativeCommand.h"
#import "AppDelegate.h"

@implementation BlinkNegativeCommand
-(id)performDefaultImplementation
{
    AppDelegate *app = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    [app badPulse:self];
    return @"Sorry";
}
@end
