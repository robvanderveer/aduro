//
//  AppDelegate.m
//  AduroCenter
//
//  Created by Rob van der Veer on 3/8/14.
//  Copyright (c) 2014 Rob van der Veer. All rights reserved.
//

#import "AppDelegate.h"
#import "SettingsWindowController.h"
#import "AduroDriver.h"
#import "NSColor+additions.h"
#include <IOKit/graphics/IOGraphicsLib.h>

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *settingsWindow;
@property (nonatomic,strong) SettingsWindowController *settingsWindowController;
@property (weak) IBOutlet NSMenu *statusbarMenu;
@property (strong, nonatomic) NSStatusItem *statusBar;

@property (strong) AduroDriver *driver;
@property (strong) NSUserNotificationCenter *userNotificationCenter;

@property bool sleeping;
@property (nonatomic, strong) ORSSerialPortManager *portManager;

@end

@implementation AppDelegate
{
    NSInteger lastBrightness;
}

-(void)awakeFromNib
{
    self.statusBar = [[NSStatusBar systemStatusBar] statusItemWithLength:NSVariableStatusItemLength];
    
    //self.statusBar.title = @"G";
    
    // you can also set an image
    self.statusBar.image = [NSImage imageNamed:@"status_error"];
    self.statusBar.alternateImage = [NSImage imageNamed:@"status_error_white"];
    
    self.statusBar.menu = self.statusbarMenu;
    self.statusBar.highlightMode = YES;
    
    _sleeping = false;
#if DEBUG
    self.debugHidden = [NSNumber numberWithBool:NO];
#else
    self.debugHidden = [NSNumber numberWithBool:YES];
#endif
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self
               selector:@selector(settingsChanged:)
                   name:NSUserDefaultsDidChangeNotification
                 object:nil];

    self.portManager = [ORSSerialPortManager sharedSerialPortManager];

    NSString *path = [[NSUserDefaults standardUserDefaults] stringForKey:@"serialPort"];
    _driver = [[AduroDriver alloc] initWithPath:path];
    [_driver setDelegate:self];
    
    //refresh data and settings.
    [self setupTimer];

    [_driver connect];
    
    //get wake and sleep notifications
    [[[NSWorkspace sharedWorkspace] notificationCenter] addObserver:self
                                                           selector:@selector(wakeFromSleep:)
                                                               name:NSWorkspaceDidWakeNotification
                                                             object:nil];
    
    [[[NSWorkspace sharedWorkspace] notificationCenter] addObserver:self
                                                           selector:@selector(goToSleep:)
                                                               name:NSWorkspaceWillSleepNotification
                                                             object:nil];
    
    [[[NSWorkspace sharedWorkspace] notificationCenter] addObserver:self
                                                           selector:@selector(goToSleep:)
                                                               name:NSWorkspaceWillPowerOffNotification
                                                             object:nil];
    
     [[NSUserNotificationCenter defaultUserNotificationCenter] setDelegate:self];

    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(serialPortsWereConnected:) name:ORSSerialPortsWereConnectedNotification object:nil];
    [nc addObserver:self selector:@selector(serialPortsWereDisconnected:) name:ORSSerialPortsWereDisconnectedNotification object:nil];
}

- (void)serialPortsWereConnected:(NSNotification *)notification
{
    NSArray *connectedPorts = [notification userInfo][ORSConnectedSerialPortsKey];
    NSLog(@"Ports were connected: %@", connectedPorts);     //collection of ORSSerialPort

    for(ORSSerialPort *port in connectedPorts)
    {
        if(_driver != NULL && [port.path isEqualToString:_driver.path])
        {
            //this was the actual driver path.
            NSLog(@"*** configured device");

            [self performSelector:@selector(connect:) withObject:NULL afterDelay:0.5];
            return;

        }
        else
        {
            NSLog(@"Something else.");
        }
    }
}

- (void)serialPortsWereDisconnected:(NSNotification *)notification
{
    NSArray *disconnectedPorts = [notification userInfo][ORSDisconnectedSerialPortsKey];
    NSLog(@"Ports were disconnected: %@", disconnectedPorts);
}


-(BOOL)userNotificationCenter:(NSUserNotificationCenter *)center shouldPresentNotification:(NSUserNotification *)notification
{
    return YES;
}

-(void)dncEvent:(NSNotification *)note
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"%@", note);
}

#pragma mark sleep/wake notifications
-(void)wakeFromSleep:(id)sender
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    _sleeping = false;

    [self updateBrightness:nil];
    [self updateSnapshot:sender];
}

-(void)goToSleep:(id)sender
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    //turn off;
    [_driver updateBrightness:1];
    lastBrightness = 0;
    _sleeping = true;
}

-(IBAction)connect:(id)sender
{
    [_driver connect];
}

-(IBAction)showPreferences:(id)sender
{
    if(_settingsWindowController == nil)
    {
        _settingsWindowController = [[SettingsWindowController alloc] initWithWindowNibName:@"SettingsWindowController"];
        _settingsWindowController.delegate = self;
    }
    [_settingsWindowController showWindow:self];
    [[_settingsWindowController window] orderFrontRegardless];
}

-(void)settingsWindowDidClose:(SettingsWindowController *)controller
{
    _settingsWindowController.delegate = nil;
    _settingsWindowController = nil;
}

-(IBAction)showAbout:(id)sender
{
    [NSApp activateIgnoringOtherApps:YES];
    [NSApp orderFrontStandardAboutPanel:self];
}

-(IBAction)goodPulse:(id)sender
{
    [_driver sendPulse:[NSColor colorWithRed:0 green:1 blue:0 alpha:1]];
}

-(IBAction)badPulse:(id)sender
{
    [_driver sendPulse:[NSColor colorWithRed:1 green:0 blue:0 alpha:1]];
}

-(void)dealloc
{
    NSDistributedNotificationCenter *dnc = [NSDistributedNotificationCenter defaultCenter];
    
    [dnc removeObserver:self name:@"com.apple.iTunes.playerInfo" object:nil];
}

-(void)settingsChanged:(id)sender
{
    NSLog(@"Settings changed");
    
    NSString *path = [[NSUserDefaults standardUserDefaults] stringForKey:@"serialPort"];
    [_driver setPath:path];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
    [_userNotificationCenter setDelegate:nil];
    _userNotificationCenter = nil;
}

-(void)applicationDidChangeScreenParameters:(NSNotification *)notification
{
    NSLog(@"did change screen");
}

- (BOOL)applicationShouldOpenUntitledFile:(NSApplication *)sender
{
    return NO;
}

-(void)setupTimer
{
    [NSTimer scheduledTimerWithTimeInterval:1.5
                                     target:self
                                   selector:@selector(updateSnapshot:)
                                   userInfo:nil
                                    repeats:YES];
    
    [NSTimer scheduledTimerWithTimeInterval:0.1
                                     target:self
                                   selector:@selector(updateBrightness:)
                                   userInfo:nil
                                    repeats:YES];

}

-(void)updateSnapshot:(id)sender
{
    if(!_sleeping && _driver.state == Synced)
    {
        [self makeSnapshot];
    }
}

-(void)updateBrightness:(id)sender
{
    bool followBrightness = [[NSUserDefaults standardUserDefaults] boolForKey:@"DimWithDisplay"];
    NSInteger brightness = [[NSUserDefaults standardUserDefaults] integerForKey:@"brightness"];
    
    //NSLog(@"brightness: %ld", b);
    if(followBrightness)
    {
        //returns a value between 0 and 1.
        float displayBrightness = [self getDisplayBrightness];
        //NSLog(@"brightness: %f",  displayBrightness );
        
        //because x 0 leaves 0, adjust a little.
        displayBrightness = 0.1 + (displayBrightness * 0.9);
        brightness = (int)((float)brightness * displayBrightness);
    
        //NSLog(@"effective brightness: %ld", b);
    }
    
    if(brightness != lastBrightness)
    {
        if(!_sleeping && _driver.state == Synced)
        {
            NSLog(@"brightness update to %ld", brightness);
            //avoid overupdating.
            [_driver updateBrightness:brightness];
            lastBrightness = brightness;
        }
    }
}

-(void)makeSnapshot
{
    CGFloat contrast = [[NSUserDefaults standardUserDefaults] floatForKey:@"contrast"];
    
    NSColor *tint =nil;
    NSData *theData=[[NSUserDefaults standardUserDefaults] dataForKey:@"tint"];
    if (theData != nil)
    {
        tint =(NSColor *)[NSUnarchiver unarchiveObjectWithData:theData];
    }
    
    CGImageRef screenShot = CGWindowListCreateImage(CGRectInfinite, kCGWindowListOptionOnScreenOnly, kCGNullWindowID, kCGWindowImageDefault);
  
    NSSize newSize = NSMakeSize(60, 3);
    NSRect newRect = NSMakeRect(0, 0, 60, 3);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGContextRef context = CGBitmapContextCreate(NULL, newSize.width, newSize.height, 8, newSize.width * 4, colorSpace, kCGBitmapByteOrderDefault | kCGImageAlphaPremultipliedLast);
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGContextScaleCTM(context, 1, 1);
    CGContextDrawImage(context, newRect, screenShot);
    
    unsigned char *data = CGBitmapContextGetData (context);
    if(data != NULL)
    {
        NSInteger width = CGBitmapContextGetWidth(context);
        NSInteger height = CGBitmapContextGetHeight(context);
        NSInteger rowBytes = CGBitmapContextGetBytesPerRow(context);
        NSInteger bitsPerPixel = CGBitmapContextGetBitsPerPixel(context);

        NSAssert( width == 60, @"Unexpected image width");
        NSAssert( height >= 1, @"Unexpected image height");
        NSAssert( bitsPerPixel == 32, @"Unexpected bits per pixel");
        
        NSMutableArray *pixelArray = [[NSMutableArray alloc] init];
        
        int row, col;
        
        row = 1;
        
        unsigned char* rowStart = (unsigned char*)(data + (row * rowBytes));
        unsigned char* nextChannel = rowStart;
        for (col = 0; col < width; col++)
        {
            unsigned char red, green, blue, alpha;
            
            red = *nextChannel;
            nextChannel++;
            green = *nextChannel;
            nextChannel++;
            blue = *nextChannel;
            nextChannel++;
            alpha = *nextChannel;
            nextChannel++;
            
            
            NSColor *color = [NSColor colorWithDeviceRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:alpha/255.0];
            if(tint != NULL && tint.alphaComponent > 0.0)
            {
                color = [self blend:color With:tint mix:tint.alphaComponent];
            }
            
            [pixelArray addObject:[color colorWithContrast:contrast]];
        }
        
        bool reverse = [[NSUserDefaults standardUserDefaults] boolForKey:@"ReverseDisplay"];
        if(reverse)
        {
            NSMutableArray *reversePixels = [[NSMutableArray alloc] initWithCapacity:pixelArray.count];
            for(NSObject *obj in pixelArray)
            {
                [reversePixels insertObject:obj atIndex:0];
            }
            pixelArray = reversePixels;
            reversePixels = nil;
        }
        
        [_driver updateWithColors:pixelArray];
        pixelArray = nil;
    }

    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    CGImageRelease(screenShot);
}

- (NSColor *)blend:(NSColor *)one With:(NSColor *)other mix:(CGFloat)percentage
{
    percentage = MAX(0,MIN(1, percentage));
    
    CGFloat r = one.redComponent * (1-percentage) + other.redComponent * percentage;
    CGFloat g = one.greenComponent * (1-percentage) + other.greenComponent * percentage;
    CGFloat b = one.blueComponent * (1-percentage) + other.blueComponent * percentage;
    
    return [NSColor colorWithRed:r green:g blue:b alpha:1];
}

-(void)blueLink:(AduroDriver *)Aduro ChangedStatus:(AduroState)state
{
    if(state == Synced)
    {
        [self updateBrightness:nil];
        self.statusBar.image = [NSImage imageNamed:@"status"];
        self.statusBar.alternateImage = [NSImage imageNamed:@"status_white"];
        
        [self showNotification:@"Connected" withSubTitle:@"The connection has been restored."];
    }
    else
    {
        self.statusBar.image = [NSImage imageNamed:@"status_error"];
        self.statusBar.alternateImage = [NSImage imageNamed:@"status_error_white"];

        if(state == Disconnected)
        {
            [self showNotification:@"Disconnected" withSubTitle:@"The connection was lost."];
        }
        else if(state == Error)
        {
            [self showNotification:@"Error" withSubTitle:@"Connection not stable ."];
        }
    }
}

-(void)showNotification:(NSString *)title withSubTitle:(NSString *)subTitle
{
    NSUserNotification *notification = [[NSUserNotification alloc] init];
    notification.title = @"Aduro";
    notification.subtitle = title;
    notification.informativeText = subTitle;
    notification.soundName = NSUserNotificationDefaultSoundName;
    [[NSUserNotificationCenter defaultUserNotificationCenter] deliverNotification:notification];
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
-(float)getDisplayBrightness
{
    io_service_t      service;
    CGDisplayErr      dErr;
    float brightness = HUGE_VALF;

    CGDirectDisplayID targetDisplay = CGMainDisplayID();
    service = CGDisplayIOServicePort(targetDisplay);
    
    // now get the brightness
    CFStringRef key = CFSTR(kIODisplayBrightnessKey);
    dErr = IODisplayGetFloatParameter(service, kNilOptions, key, &brightness);
    if (dErr == kIOReturnSuccess)
    {
        return brightness;
    }
    else
    {
        return HUGE_VALF;
    }
}
#pragma GCC diagnostic pop


@end
