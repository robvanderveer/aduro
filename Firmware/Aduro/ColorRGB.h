#ifndef COLORRGB_H
#define COLORRGB_H

struct ColorRGB
{
  uint8_t r;
  uint8_t g;
  uint8_t b;
  
  ColorRGB()
  {
  }
  
  ColorRGB(uint8_t r, uint8_t g, uint8_t b)
  {
    this->r = r;
    this->g = g;
    this->b = b;
  }
  
  // blend with alpha. The alpha is the opacity of the new color, so alpha 1 is full Color B.
  ColorRGB blendWith(ColorRGB b, float alpha)
  {
      uint8_t newR = ((float)this->r * (1-alpha)) + ((float)b.r * (alpha));
      uint8_t newG = ((float)this->g * (1-alpha)) + ((float)b.g * (alpha));
      uint8_t newB = ((float)this->b * (1-alpha)) + ((float)b.b * (alpha ));
      
      return ColorRGB(newR, newG, newB);
  }
  
  uint32_t toColor()
  {
    return ((uint32_t)this->r << 16) | ((uint32_t)this->g << 8) | (uint32_t)this->b;
  }
};

#endif
