//
//  SettingsWindowController.h
//  AduroCenter
//
//  Created by Rob van der Veer on 4/8/14.
//  Copyright (c) 2014 Rob van der Veer. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ORSSerialPort.h"
#import "ORSSerialPortManager.h"

@class SettingsWindowController;

@protocol SettingsDelegate<NSObject>
-(void)settingsWindowDidClose:(SettingsWindowController *)controller;
@end

@interface SettingsWindowController : NSWindowController<NSWindowDelegate>
@property (nonatomic, strong) ORSSerialPortManager *portManager;
@property (nonatomic, assign) id<SettingsDelegate> delegate;
@end
