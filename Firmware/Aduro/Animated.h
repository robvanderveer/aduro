#ifndef ANIMATED_H
#define ANIMATED_H

#include "Arduino.h"
#include "Easing.h"

template <class T> class Animated
{
  private:
    T fromValue;
    T toValue;
    Easing *ease;
    unsigned long duration;
    unsigned long timeStarted;
    bool completed;
  
  public:
    Animated(Easing *e, unsigned long duration, T from, T to)
    {
      if(!e)
      {
         //e = Easing(Easing::easeLinear, In); 
      }
      this->ease = e;
      this->duration = duration;
      this->fromValue = from;
      this->toValue = to;
    };
    
    void setToValue(T value)
    {
      //capture the current value as the fromValue;
      this->fromValue = getValue();
      this->toValue = value;
      
      //reset timing;
      this->completed = false;
      this->timeStarted = millis();  
    };
    
    bool reset(T fromValue, T toValue)
    {
      this->fromValue = fromValue;
      this->toValue = toValue;
      this->completed = false;
      this->timeStarted = millis();  
    };
    
    bool getCompleted()
    {
     return completed; 
    }
    
  //auto restarts
    T getValue()
    {
      if(completed)
      {
        //save time.
        return this->toValue;
      }
      
      //how many time has passed?
      unsigned long passed = millis() - this->timeStarted;
      
      //value clamped.
      if(passed < 0)
      {
        passed = 0;
      }
      else if(passed > duration)
      {
        passed = duration;
        completed = true;
      }
      
      float animationPosition = (float)passed / (float)duration;  
      float easedValue = this->ease->ease(animationPosition);
      return ((toValue - fromValue) * easedValue)  + fromValue;
      
      //return toValue;
    };
};


#endif
