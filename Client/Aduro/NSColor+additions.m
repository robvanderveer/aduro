//
//  NSColor+additions.m
//  Aduro
//
//  Created by Rob van der Veer on 26/8/14.
//  Copyright (c) 2014 Rob van der Veer. All rights reserved.
//

#import "NSColor+additions.h"

@implementation NSColor (Aduro)
-(NSColor *)colorWithContrast:(CGFloat)contrast
{
    CGFloat r = [self adjust:self.redComponent withContrast:contrast];
    CGFloat g = [self adjust:self.greenComponent withContrast:contrast];
    CGFloat b = [self adjust:self.blueComponent withContrast:contrast];
    
    return [NSColor colorWithRed:r green:g blue:b alpha:self.alphaComponent];
}


-(CGFloat)adjust:(CGFloat)value withContrast:(CGFloat)contrast
{
    float adjusted = (contrast * (value - 0.5)) + 0.5;
    return MIN(MAX(adjusted, 0),1);
}

@end
