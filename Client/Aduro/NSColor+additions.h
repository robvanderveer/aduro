//
//  NSColor+additions.h
//  Aduro
//
//  Created by Rob van der Veer on 26/8/14.
//  Copyright (c) 2014 Rob van der Veer. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSColor (Aduro)

-(NSColor *)colorWithContrast:(CGFloat)contrast;

@end
