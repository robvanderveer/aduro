//
//  AduroDriver.h
//  AduroCenter
//
//  Created by Rob van der Veer on 3/8/14.
//  Copyright (c) 2014 Rob van der Veer. All rights reserved.
//

#import "ORSSerialPort.h"
#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

@class AduroDriver;

typedef NS_ENUM(NSInteger, AduroState)
{
    Closed,
    Connecting,
    Connected,
    Synced,
    Disconnected,
    Error
};

@protocol AduroDelegate <NSObject>
-(void)blueLink:(AduroDriver *)Aduro ChangedStatus:(AduroState)state;
@end

@interface AduroDriver : NSObject<ORSSerialPortDelegate>

@property (nonatomic, strong) NSString *path;
@property (readonly, strong) ORSSerialPort *port;
@property (nonatomic,assign) id<AduroDelegate> delegate;
@property (readonly) AduroState state;

-(instancetype)initWithPath:(NSString *)path;
/// send an array of RGB values.
-(void)updateWithColors:(NSArray *)colors;
-(void)updateBrightness:(NSInteger)level;
-(void)sendPulse:(NSColor *)color;
-(void)connect;
@end
