//
//  BlinkNegativeCommand.h
//  Aduro
//
//  Created by Rob van der Veer on 22/8/14.
//  Copyright (c) 2014 Rob van der Veer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BlinkNegativeCommand : NSScriptCommand
-(id)performDefaultImplementation;
@end
