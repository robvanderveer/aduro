Aduro
=====

Bluetooth controlled NeoPixel LED strip

- Minimal Arduino running @8Mhz internal clock
- 5V, USB powered
- 1000 uF capacitor between GND and 5V
- 60 leds NeoPixel strip, 30leds/m
- Firmware programmed via ICMP
- Extern power available on the LED strip (see below)
- JY-MCU (HC-06) Bluetooth serial port module

## Power
Running this over USB is tight. When 60 leds burn bright white, it is drawing (RGB) 3 * 20mA * 60 = 3600mAmps, which clearly is way too much for a basic USB port.

The current sketch tries to keep the current low by only doing 50% blue by default, meaning 10mA*60 = still 600mA which is still pushing it to the edge.

The current prototype is no longer using the USB port but a full 3A 5V adapter, able to fully light the string for a short time.

## Schematic

The `Hardware` folder contains the EAGLE schematic and a PNG of the latest diagram for quick reference. It's too freakin' easy.

## Sketch
The sketch is pretty basic.

Four utility classes are used: 
- `TimedEvent` is a helper class to do away with all the `millis()` calculations. 
- `Easing` is a helper class to interpolate a value according to a curve.
- `Animated<T>` a template class that will interpolate between values over time with a specified easing pattern.
- `ColorRGB` is a structure to help with blending RGB values and alpha animations. the `toColor()` method turns it into an `uint32_t` that can be using in [Adafruit's NeoPixel library](https://learn.adafruit.com/adafruit-neopixel-uberguide/arduino-library).

In the main loop several timers and counters are maintain to animate the colors.

Basic serial protocol. It is a binary protocol to safe precious bandwidth:

- A command starts with 0x78
- Followed by the package length (8bits),
- Followed by the package payload comes (package length * 8 bits),
- The the checksum byte to verify the payload. The checksum is a 8bit value, calculated by adding all the bytes together ignoring overflow.

A package consists of one command byte followed by optional data. If the package size matches the command `OK<C>` is returned where <C> is the name of the command. The following commands are supported:

- 'C' sets all colors, follow by 60x3 8bit integers. Package size 181.
- 'B' sets brightness. Valid numbers follows, 1 through 100. Package size 2
- 'I' responds with `+Aduro ready` after the `OKI` message. Package size 1

If the command is not supported the Aduro returns `ERRC`.

## Client

The client app is an OS X Mavericks 10.9.4 Statusbar app with a preferences pane. Currently you can adjust:
- The brightness
- The Serial port 
- Follow display brightness.
- Direction (depends on which side your led strips starts, left to right or right to left)

The client takes a screenshot every 1 second, scales it down to 60x1, and sends the colors off to the Aduro chip over serial. The arduino then takes over.

To be able to run the appleScripts from xcode, do a `chmod +x` on the .applescript files.