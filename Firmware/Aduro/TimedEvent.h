#ifndef TIMEDEVENT_H
#define TIMEDEVENT_H

#include "arduino.h"

struct timedEvent
{
    unsigned long duration;
    unsigned long lastEvent;
    
    bool hasElapsed()
    {
       return hasElapsed(duration, true);
    }
    
    //defaults to 1 second.
    timedEvent()
    {
       lastEvent = millis(); 
       duration = 1000;
    }
    
    bool hasElapsed(unsigned long t)
    {
      return hasElapsed(t, true);
    }
    
    bool hasElapsed(unsigned long t, bool autoReset)
    {
      bool elapsed = (millis() - lastEvent) > t; 
      if(elapsed && autoReset)
      {
          reset(t);
      }
      return elapsed;
    }
    
    void reset()
    {
       lastEvent = millis(); 
    } 
    
    void reset(unsigned long d)
    {
       duration = d;
       reset(); 
    }
};

#endif
