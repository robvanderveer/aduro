//
//  BlinkPositiveCommand.m
//  Aduro
//
//  Created by Rob van der Veer on 22/8/14.
//  Copyright (c) 2014 Rob van der Veer. All rights reserved.
//

#import "BlinkPositiveCommand.h"
#import "AppDelegate.h"

@implementation BlinkPositiveCommand
-(id)performDefaultImplementation
{
    AppDelegate *app = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    [app goodPulse:self];
    return @"Ok";
}
@end
