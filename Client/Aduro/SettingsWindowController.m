//
//  SettingsWindowController.m
//  AduroCenter
//
//  Created by Rob van der Veer on 4/8/14.
//  Copyright (c) 2014 Rob van der Veer. All rights reserved.
//

#import "SettingsWindowController.h"

@interface SettingsWindowController ()
@end

@implementation SettingsWindowController

- (void)windowDidLoad {
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    
    
    self.portManager = [ORSSerialPortManager sharedSerialPortManager];
    self.window.delegate = self;
    NSLog(@"Window loaded");
    
    [[NSColorPanel sharedColorPanel] setShowsAlpha:YES];
}

-(void)dealloc
{
    self.window.delegate = nil;
    
    if(_portManager)
    {
        self.portManager = nil;
    }
}

-(void)windowWillClose:(NSNotification *)notification
{
    NSLog(@"Closing the window now.");
    
    if([self.delegate respondsToSelector:@selector(settingsWindowDidClose:)])
    {
        [self.delegate settingsWindowDidClose:self];
    }
}

- (void)setSerialPortManager:(ORSSerialPortManager *)manager
{
    if (manager != _portManager)
    {
        [_portManager removeObserver:self forKeyPath:@"availablePorts"];
        _portManager = manager;
        NSKeyValueObservingOptions options = NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld;
        [_portManager addObserver:self forKeyPath:@"availablePorts" options:options context:NULL];
    }
}

@end
