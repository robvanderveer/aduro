#ifndef EASING_H
#define EASING_H

enum EasingMode
{
    In,
    Out,
    InOut
};

typedef float (*EasingFunction)(float t);

// use this class to interpolate value over time (0..t) 
// depending on the mode it will provide easein, easeout or easeinout.
class Easing
{
  private:
    EasingMode mode;
    EasingFunction f;
    
  public:
    Easing(EasingFunction function, EasingMode mode);
    float ease(float t);
  
  static float easeLinear(float t);
  static float easeCubic(float t);
  static float easeQuart(float t);
  static float easeQuint(float t);
  static float easeSinus(float t);
};

 

#endif
