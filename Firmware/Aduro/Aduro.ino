/*
  Aduro
  
  Experimental firmware for bluetooth operated LED lights based on NEOPixel.
 
  Burn with: [USBASP] ATMega328p @16Mhz Resonator, no bootloader.
  
 */
 
#include "TimedEvent.h"
#include "Easing.h"
#include "ColorRGB.h"
#include "Animated.h"
#include <Adafruit_NeoPixel.h>

const int PIN_Led = 13;      //default LED pin for Arduino, very ambigious
const int PIN_NeoPixel = 6;  //must be a PWM pin.

const int8_t CMDSTART = 0x78;

Adafruit_NeoPixel strip = Adafruit_NeoPixel(60, PIN_NeoPixel, NEO_GRB + NEO_KHZ800);

timedEvent noResponseEvent;
timedEvent noDataEvent;

ColorRGB colors[2][60];
ColorRGB black;
ColorRGB pulseColor;

int frame = 0;
int maxFrame = 512;

Easing blendEasing = Easing(Easing::easeCubic, In);
Easing pulseEasing = Easing(Easing::easeCubic, Out);
Easing brightnessEasing = Easing(Easing::easeCubic, Out);

Animated<float> cloudAnimation = Animated<float>(0, 5000, 0, 1);
Animated<float> brightnessAnimation = Animated<float>(&brightnessEasing, 400, 0.2, 0.2);
Animated<float> blendAnimation = Animated<float>(&blendEasing, 1300, 0, 1);
Animated<float> pulseAnimation = Animated<float>(&pulseEasing, 2500, 0, 1);

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(PIN_Led, OUTPUT);  

   
  Serial.begin(57600);  
  while(!Serial)
  {
    
  }
  Serial.println("+ADURO RESET");
  
  digitalWrite(PIN_Led, true);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'

  digitalWrite(PIN_Led, false);
  
  for(uint16_t p=0; p<strip.numPixels(); p++) 
      {
        
        //final brightness blend.
        ColorRGB finalBrightness = ColorRGB(10, 10, 20);
        strip.setPixelColor(p, finalBrightness.toColor());
        strip.show();
        delay(10);
      }
   
      delay(500);

      noDataEvent.reset();

}

// the loop routine runs over and over again forever:
void loop() 
{
  while(Serial.available() > 0)
  {
     processIncomingBinary(Serial.read(), false); 
  }

  if(noDataEvent.hasElapsed(5000))
  {
    Serial.println("IDLE");
    processIncomingBinary(0, true);
  }
    
  if(cloudAnimation.getCompleted())
  {
    cloudAnimation.reset(0,1);
  }
  
  if(blendAnimation.getCompleted())
  {   
    //we're at the end. Copy the colors back to the other buffer.
    for(int i = 0; i < 60; i++)
    {
       colors[0][i] = colors[1][i]; 
    }
  }
    
  if(noResponseEvent.hasElapsed(30*1000, false))
  {
     Serial.println("SHUTDOWN");
     //set brightness to 0, effectively turning it off.
     brightnessAnimation.setToValue(0);
  }
   
    //float angle = cloudAnimation.getValue();  //0..1
    frame++;
    if(frame > maxFrame)
    {
      frame = 0;
    }
    
    float fadeFactor = blendAnimation.getValue();
    float brightness = brightnessAnimation.getValue();
    float pulseFactor = pulseAnimation.getValue();
      
    //update strips.
    for(uint16_t p=0; p<strip.numPixels(); p++) 
    {
      ColorRGB useColor = colors[0][p];
      ColorRGB targetColor = colors[1][p];
      ColorRGB mixed = useColor.blendWith(targetColor, fadeFactor);
      
      //angle+=0.04;
      float sinusValue = Easing::easeSinus( ((float)((frame + p*2) % maxFrame))/(float)maxFrame);
      
      //mix it with the 'cloud effect.
      mixed = mixed.blendWith(black, sinusValue * 0.8);
      
      //are we currently pulsing?
      //fade in the pulse color. 0 = full pulse Color, 1 = full temp.
      mixed = pulseColor.blendWith(mixed, pulseFactor);
      
      //final brightness blend.
      ColorRGB finalBrightness = ColorRGB(mixed.r * brightness, mixed.g * brightness, mixed.b * brightness);
      strip.setPixelColor(p, finalBrightness.toColor());
    }
    strip.show();
}

#define MAX_BUFFER 512

// instead of reading ASCII packets, go with a binary message format. 
// a packet consists of a header character, '0x78'
// followed by a payload length n
// followed by 'n' unsigned bytes
// closed by a checksum (all payload bytes). 
// the packet will be dropped if the crc fails, before the packet is executed.

void processIncomingBinary(uint8_t character, bool reboot)
{
  static uint8_t state = 0;
  static int bufferPos = 0;
  static uint8_t buffer[MAX_BUFFER];
  static uint8_t packetsize = 0;
  static uint8_t checksum = 0;

  if(reboot)
  {
          state = 0;
          bufferPos = 0;
          checksum = 0;
          packetsize = 0;    
          return;
  }
  
  switch(state)
  {
     case 0:
        if(character == CMDSTART)
        {
          state = 1;
          bufferPos = 0;
          checksum = 0;
          packetsize = 0;
        }
        else
        {
          /* echo the data */
          Serial.write((char)character);
          //Serial.println();
        }
        break; 
     case 1:
        packetsize = character;
        checksum = 0;
        state = 2;
        break;
     case 2:
        digitalWrite(PIN_Led, (bufferPos&&1));    //show we are reading Data.
  
        buffer[bufferPos++] = character;
        checksum += character;
        if(bufferPos == packetsize)
        {
          state = 3;
        }
        break;
     case 3:
        if(checksum == character)
        {
           digitalWrite(PIN_Led, true);
           //process command
           processCommandBinary(buffer, packetsize); 
           noResponseEvent.reset();
           noDataEvent.reset();

           //done processing.
           digitalWrite(PIN_Led, false);    
        }
        else
        {
          Serial.println("CRC error");
        }
        state = 0;
        break;
     default:
        Serial.write("!");
  }
}

void processCommandBinary(uint8_t *buf, int packetsize)
{
  if(buf[0] == 'C' && packetsize == (60*3)+1)
  {
    Serial.println("OKC");
    processSetColorsPacked(buf);
  }
  else if(buf[0] == 'B' && packetsize == 2)
  {
     Serial.println("OKB");
     processSetBrightness(buf); 
  }
  else if(buf[0] == 'I' && packetsize == 1)
  {
    Serial.println("OKI");
    Serial.println("+Aduro ready");
  }
  else if(buf[0] == 'P' && packetsize == 4)
  {
    Serial.println("OKP");
    processPulse(buf);
  }
  else
  {
    Serial.print("ERR");
    Serial.println((char)buf[0]);
  }
}

void processSetBrightness(uint8_t *buf)
{
   float val;
   
   val = buf[1];
   
   //clamp
   val = min(max(1, val), 100);
   
   brightnessAnimation.setToValue(val / 100.0);
}

void processPulse(uint8_t *buf)
{
  int pos = 1;    //ignore 'P';

  uint8_t r,g,b;
  r = buf[pos++];
  g = buf[pos++];
  b = buf[pos++];
    
  pulseColor = ColorRGB(r,g,b);
  pulseAnimation.reset(0,1);
}

//reads colors into the new color buffer.
void processSetColorsPacked(uint8_t *buf)
{  
  int pos = 1;    //ignore 'C';
 
  if(blendAnimation.getCompleted() == false)
  {
    //ignore this update if we are still busy.
    return;
  }

  blendAnimation.reset(0, 1);
  
  //read 60 colors
  for(int i = 0; i < 60; i++)
  {
    uint8_t r,g,b;
    r = buf[pos++];
    g = buf[pos++];
    b = buf[pos++];
    
    ColorRGB color = ColorRGB(r,g,b);
    colors[1][i] = color;
  }
}


